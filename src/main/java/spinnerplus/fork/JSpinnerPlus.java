/**
 * @author JohnnyTB (jtrejosb@live.com)
 * gitlab.com/iolinker | github.com/jtrejosb
 *
 * Licenses GNU GPL v3.0 and Eclipse Public License 2.0
 * Date: 03/08/2022, Time: 20:08:26
 */
package spinnerplus.fork;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

/**
 * A single line input field that lets the user select a number or an object
 * value from an ordered sequence and optionally display a descriptive string
 * in the input field editor.
 */
public class JSpinnerPlus extends JSpinner implements ChangeListener {
  private JFormattedTextField ftf;
  private String descriptionText="";
  private Color descColor=Color.GRAY;

  /**
   * Creates a new <code>JSpinnerPlus</code> with a default instance of
   * {@link SpinnerNumberModel} (that is, a model with value 0, step size 1,
   * and no upper or lower limit).
   *
   * @see javax.swing.SpinnerNumberModel
   */
  public JSpinnerPlus() {
    initializeComponent();
  }

  /**
   * Creates a new <code>JSpinnerPlus</code> with a descriptive text displayed in
   * the editor acting as an permanent placeholder.
   *
   * @param descriptionText the string displayed.
   */
  public JSpinnerPlus(String descriptionText) {
    this.descriptionText=descriptionText;
    initializeComponent();
  }

  /**
   * Creates a new <code>JSpinnerPlus</code> with the descriptive text and the
   * specified model. The {@link #createEditor(SpinnerModel)} method is used to create an editor
   * that is suitable for the model.
   *
   * @param descriptionText the string displayed.
   *
   * @param model the model (<code>null</code> not permitted).
   *
   * @throws NullPointerException if <code>model</code> is <code>null</code>.
   */
  public JSpinnerPlus(String descriptionText,SpinnerNumberModel model) {
    super(model);
    this.descriptionText=descriptionText;
    initializeComponent();
  }

  /**
   * Creates the text field used to display the current value and the decription
   * text in the spinner. This method is invoked by all constructors.
   */
  private void initializeComponent() {
    ftf=new JFormattedTextField(getValue()) {
      @Override
      public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(descColor);
        g.drawString(descriptionText,6,18);
      }
    };
    ftf.setHorizontalAlignment(SwingConstants.RIGHT);
    ftf.setEditable(false);

    addChangeListener(this);
    setEditor(ftf);
  }

  /**
   * Sets the foreground color for the displayed description text.
   *
   * @param descColor The color (<code>null</code> permitted).
   */
  public void setDescriptionColor(Color descColor) {
    if(descColor!=null)
      this.descColor=descColor;
    revalidate();
  }

  /**
   * Sets the text for the description in the editor field.
   *
   * @param descriptionText The text (<code>null</code> permitted).
   */
  public void setDescription(String descriptionText) {
    if(descriptionText!=null)
      this.descriptionText=descriptionText;
    revalidate();
  }

  @Override
  public void stateChanged(ChangeEvent evt) {
    ftf.setValue(getModel().getValue());
  }
}
